#ifndef JOYSTICKCODES_H
#define JOYSTICKCODES_H


enum{

 JOY_XBOX_A   = 0,
 JOY_XBOX_B   = 1,
 JOY_XBOX_X   = 2,
 JOY_XBOX_Y   = 3,
 JOY_XBOX_BUMPER_RIGHT    = 4,
 JOY_XBOX_BUMPER_LEFT   = 5,
 JOY_XBOX_STICK_RIGHT   = 6,
 JOY_XBOX_STICK_LEFT    = 7,
 JOY_XBOX_BACK    = 8,
 JOY_XBOX_START   = 9,
 JOY_XBOX_PAD_RIGHT   = 10,
 JOY_XBOX_PAD_LEFT    = 11,
 JOY_XBOX_PAD_DOWN    = 12,
 JOY_XBOX_PAD_UP    = 13,
 JOY_NONE = 14,

};

enum{

 LEFT_STICK_LEFT      = 0,
 LEFT_STICK_RIGHT     = 1,
 LEFT_STICK_UP        = 2,
 LEFT_STICK_DOWN      = 3,
 LEFT_TRIGGER_DOWN    = 4,
 RIGHT_TRIGGER_DOWN   = 5,
 RIGHT_STICK_LEFT     = 6,
 RIGHT_STICK_RIGHT    = 7,
 RIGHT_STICK_UP       = 8,
 RIGHT_STICK_DOWN     = 9,
 DPAD_LEFT            = 10,
 DPAD_RIGHT           = 11,
 DPAD_UP              = 12,
 DPAD_UP2             = 13,
 DPAD_DOWN            = 14,
 NONE                 = 15,



};

#endif
