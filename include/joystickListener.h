#ifndef JOYSTICKLISTENER_H
#define JOYSTICKLISTENER_H

#define JOY_MAX_STICKS   8
#define JOY_MAX_AXES     3
#define JOY_MAX_BUTTONS  15

#include <iostream>
#include <allegro5/allegro.h>
#include <JoystickCodes.h>


class joystickListener
{
  public:
    joystickListener();
    virtual ~joystickListener();

    void on_event( ALLEGRO_EVENT_TYPE event_type, int keycode);
    void update();

    static bool button[JOY_MAX_BUTTONS];
    static bool buttonPressed[JOY_MAX_BUTTONS];
    static bool buttonReleased[JOY_MAX_BUTTONS];
    static bool anyButtonPressed;
    static bool anyStickPressed;
    static bool anyButtonReleased;

    static int lastButtonPressed;
    static int lastButtonReleased;
    static bool stickDirections[20];
    static void clearButtons();

  protected:
  private:
    static bool lastTicksButton[JOY_MAX_BUTTONS];
    static ALLEGRO_JOYSTICK_STATE joyState;
};

#endif // JOYSTICKLISTENER_H
